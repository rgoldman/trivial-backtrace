(in-package #:trivial-backtrace-test)

(in-suite trivial-backtrace-test)

;;; hahaha -- this test doesn't work on SBCL, because it detects the
;;; error at compilation time!!!
(test generates-backtrace
  (let ((output nil))
    (handler-case
        (error "Here's a bogus error")
      (error (c)
        (setf output (print-backtrace c :output nil))))
    (is-true (stringp output))
    (is (plusp (length output)))))
