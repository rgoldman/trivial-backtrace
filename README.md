# Trivial-Backtrace

  * [Documentation](#api)
  * [News](#what-is-happening)

This is my (Robert Goldman's) fork of Gary King's original
trivial-backtrace library, which now emits deprecation warnings on
SBCL, and for which there is no longer an active maintainer as far as
I can tell.

## What it is

On of the many things that didn't quite get into the Common
Lisp standard was how to get a Lisp to output its call stack
when something has gone wrong. As such, each Lisp has
developed its own notion of what to display, how to display
it, and what sort of arguments can be used to customize it.
`trivial-backtrace` is a simple solution to generating a
backtrace portably. Toi the best of my knowledge, it supports Allegro Common
Lisp, LispWorks, ECL, MCL, SCL, SBCL and CMUCL. Its
interface consists of three functions and one variable:

 * `print-backtrace`
 * `print-backtrace-to-stream`
 * `print-condition`
 * `*date-time-format*`

You can probably already guess what they do, but they are
described in more detail below.

## API

### `PRINT-BACKTRACE`

`print-backtrace error &key (output *debug-io*)  (if-exists :append)  (verbose nil)`

Send a backtrace for the error `error` to `output`. 

The keyword arguments are:

 * `:output` - where to send the output. This can be:

     * a string (which is assumed to designate a pathname)
     * an open stream
     * nil to indicate that the backtrace information should be 
       returned as a string

 * `if-exists` - what to do if output designates a pathname and 
   the pathname already exists. Defaults to `:append.`

 * `verbose` - if true, then a message about the backtrace is sent
   to `*terminal-io*`. Defaults to `nil`.

If the `output` is `nil,` then returns the backtrace output as a
string. Otherwise, returns `nil`.

### `PRINT-BACKTRACE-TO-STREAM`

`print-backtrace-to-stream stream`

Send a backtrace of the current error to `stream`. 

`Stream` is assumed to be an open writable file stream or a
`string-output-stream`. Note that `print-backtrace-to-stream`
will print a backtrace for whatever the Lisp deems to be the 
*current* error.


### `PRINT-CONDITION`

`print-condition (condition stream)`

Print `condition` to `stream` using the pretty printer.

### `*DATE-TIME-FORMAT*` Variable

The default format to use when printing dates and times.

* %% - A '%' character
* %d - Day of the month as a decimal number [01-31]
* %e - Same as %d but does not print the leading 0 for days 1 through 9 
     [unlike strftime[], does not print a leading space]
* %H - Hour based on a 24-hour clock as a decimal number [00-23]
*%I - Hour based on a 12-hour clock as a decimal number [01-12]
* %m - Month as a decimal number [01-12]
* %M - Minute as a decimal number [00-59]
* %S - Second as a decimal number [00-59]
* %w - Weekday as a decimal number [0-6], where Sunday is 0
* %y - Year without century [00-99]
* %Y - Year with century [such as 1990]

This code is borrowed from the `format-date` function in
metatilities-base.

## What is happening

[2019/04/22:rpg] -- Created this fork, updating ASDF definitions,
replacing a deprecated SBCL function, and replacing use of LIFT test
library with FiveAM.
