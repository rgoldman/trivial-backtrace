(in-package #:common-lisp-user)

(defpackage #:trivial-backtrace-system (:use #:asdf #:cl))
(in-package #:trivial-backtrace-system)

(defsystem trivial-backtrace
  :version "1.2.0"
  :author "Gary Warren King <gwking@metabang.com> and contributors"
  :maintainer "Robert P. Goldman <rpgoldman@sift.net>"
  :licence "MIT Style license "
  :description "trivial-backtrace"
  :depends-on ()
  :components
  ((:static-file "COPYING")
   (:module 
    "setup"
    :pathname "dev/"
    :components ((:file "packages")))
   (:module 
    "dev"
    :depends-on ("setup")
    :components ((:file "utilities")
                 (:file "backtrace")
                 (:file "map-backtrace")
                 (:file "fallback" :depends-on ("backtrace" "map-backtrace")))))
  :in-order-to ((test-op (test-op trivial-backtrace/test))))

(defsystem trivial-backtrace/test
  :author "Gary Warren King <gwking@metabang.com>"
  :maintainer "Robert P. Goldman <rpgoldman@sift.net>"
  :licence "MIT Style License; see file COPYING for details"
  :components ((:module 
                "setup"
                :pathname "test/"
                :components ((:file "packages")
                             (:file "test-setup"
                                    :depends-on ("packages"))))
               (:module 
                "test"
                :pathname "test/"
                :depends-on ("setup")
                :components ((:file "tests"))))
  :perform (test-op :after (op c)
                    (funcall
                     (intern (symbol-name '#:run!) :fiveam)
                     (intern (symbol-name '#:trivial-backtrace-test) 'trivial-backtrace-test)))
  :depends-on (:fiveam :trivial-backtrace))


